from setuptools import setup, find_packages
setup(name="ddmpy",
      version="0.1",
      author="HiePACS",
      description="linear algebra package for parallel domain decomposition methods",
      url="https://gitlab.inria.fr/gmarait/ddmpy",
      license="CeCILL-C",
      packages=find_packages(),
      py_modules=["ddmpy"]
)
